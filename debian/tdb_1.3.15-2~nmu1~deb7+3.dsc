Format: 3.0 (quilt)
Source: tdb
Binary: libtdb1, libtdb1-dbg, tdb-tools, libtdb-dev, python-tdb, python-tdb-dbg
Architecture: linux-any kfreebsd-any
Version: 1.3.15-2~nmu1~deb7+3
Maintainer: Debian Samba Maintainers <pkg-samba-maint@lists.alioth.debian.org>
Uploaders: Jelmer Vernooĳ <jelmer@debian.org>, Mathieu Parent <sathieu@debian.org>
Homepage: http://tdb.samba.org/
Standards-Version: 4.0.0
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-samba/tdb.git
Vcs-Git: https://anonscm.debian.org/git/pkg-samba/tdb.git
Build-Depends: debhelper (>= 9), docbook-xml, docbook-xsl, python-dev (>= 2.6.6-3), xsltproc
Package-List: 
 libtdb-dev deb libdevel optional
 libtdb1 deb libs optional
 libtdb1-dbg deb debug extra
 python-tdb deb python optional
 python-tdb-dbg deb debug extra
 tdb-tools deb utils optional
Checksums-Sha1: 
 0bdf33c1bd27922fcb7a71faf826bd7d500c256e 502627 tdb_1.3.15.orig.tar.gz
 15145f760c2bde6af87155fc84545b90ad2a241e 21124 tdb_1.3.15-2~nmu1~deb7+3.debian.tar.gz
Checksums-Sha256: 
 b4a1bf3833601bd9f10aff363cb750860aef9ce5b4617989239923192f946728 502627 tdb_1.3.15.orig.tar.gz
 39f0c9cff140d7121a9edd337187e0fab3288fdf11080a18997e6609278fed5e 21124 tdb_1.3.15-2~nmu1~deb7+3.debian.tar.gz
Files: 
 60ece3996acc8d85b6f713199da971a6 502627 tdb_1.3.15.orig.tar.gz
 e1c34a25b88e70a384fd174109a196db 21124 tdb_1.3.15-2~nmu1~deb7+3.debian.tar.gz
