Format: 3.0 (quilt)
Source: ldb
Binary: libldb1, libldb1-dbg, ldb-tools, libldb-dev, python-ldb, python-ldb-dev, python-ldb-dbg
Architecture: any
Version: 2:1.2.3-1~nmu1~deb7+3
Maintainer: Debian Samba Maintainers <pkg-samba-maint@lists.alioth.debian.org>
Uploaders: Jelmer Vernooij <jelmer@debian.org>, Mathieu Parent <sathieu@debian.org>
Homepage: http://ldb.samba.org/
Standards-Version: 4.0.0
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-samba/ldb.git
Vcs-Git: https://anonscm.debian.org/git/pkg-samba/ldb.git
Build-Depends: debhelper (>> 9), docbook-xml, docbook-xsl, libcmocka-dev (>= 1.1.1~), libldap2-dev, libpopt-dev, libtalloc-dev (>= 2.1.10~), libtdb-dev (>= 1.3.15~), libtevent-dev (>= 0.9.33~), pkg-config, python (>= 2.6.6-3), python-all-dbg (>= 2.6.6-3), python-all-dev (>= 2.6.6-3), python-talloc-dev (>= 2.1.10~), python-tdb (>= 1.3.15~), xsltproc
Package-List: 
 ldb-tools deb utils optional
 libldb-dev deb libdevel optional
 libldb1 deb libs optional
 libldb1-dbg deb debug extra
 python-ldb deb python optional
 python-ldb-dbg deb debug extra
 python-ldb-dev deb libdevel optional
Checksums-Sha1: 
 4a1c68b8fdd28fc8a219fa06cfa622016469a909 1344513 ldb_1.2.3.orig.tar.gz
 4e4969d53e1c13cf1bc93194c3171836eed6e444 19759 ldb_1.2.3-1~nmu1~deb7+3.debian.tar.gz
Checksums-Sha256: 
 dcbdcf24cebd52c8878af935e2474df7df67d16b2b6856a34baef3274d9f6d00 1344513 ldb_1.2.3.orig.tar.gz
 3ddd176ea1fe1b140ee2b7d1d0efa2b1c9364e13fff42411f668de3a684ea157 19759 ldb_1.2.3-1~nmu1~deb7+3.debian.tar.gz
Files: 
 947239ab0ca45163518735e70abdade4 1344513 ldb_1.2.3.orig.tar.gz
 ab237da21fee1b289f227a2c7483720d 19759 ldb_1.2.3-1~nmu1~deb7+3.debian.tar.gz
