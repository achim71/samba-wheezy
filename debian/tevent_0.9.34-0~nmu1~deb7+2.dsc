Format: 3.0 (quilt)
Source: tevent
Binary: libtevent0, libtevent0-dbg, libtevent-dev
Architecture: any
Version: 0.9.34-0~nmu1~deb7+2
Maintainer: Debian Samba Maintainers <pkg-samba-maint@lists.alioth.debian.org>
Uploaders: Jelmer Vernooij <jelmer@debian.org>, Mathieu Parent <sathieu@debian.org>
Homepage: https://tevent.samba.org/
Standards-Version: 4.0.0
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-samba/tevent.git
Vcs-Git: https://anonscm.debian.org/git/pkg-samba/tevent.git
Build-Depends: debhelper (>= 9), libaio-dev [linux-any], libtalloc-dev (>= 2.1.10~), pkg-config, python, python-all-dev (>= 2.6.6-3), python-talloc-dev (>= 2.1.10~)
Package-List: 
 libtevent-dev deb libdevel optional
 libtevent0 deb libs optional
 libtevent0-dbg deb debug extra
Checksums-Sha1: 
 ed21eea3e85ecbe4320586330a7d3c8c13bfe3e8 590260 tevent_0.9.34.orig.tar.gz
 7af086a9449b03d286f09d3a9fe06184644fd779 13420 tevent_0.9.34-0~nmu1~deb7+2.debian.tar.gz
Checksums-Sha256: 
 73213ef8b27f4a0164e375140a177a751e06fe190a90f3178e24f206b4747b8a 590260 tevent_0.9.34.orig.tar.gz
 40215b92d3b6c4a795545e2a3c083507311b74c02608d9d39ec775e9b9f89455 13420 tevent_0.9.34-0~nmu1~deb7+2.debian.tar.gz
Files: 
 8df1ae19fd991b82abe66889da0651b0 590260 tevent_0.9.34.orig.tar.gz
 72ff2e31846817dcac59aa945576ddc6 13420 tevent_0.9.34-0~nmu1~deb7+2.debian.tar.gz
