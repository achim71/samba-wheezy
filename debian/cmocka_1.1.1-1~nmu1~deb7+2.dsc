Format: 3.0 (quilt)
Source: cmocka
Binary: libcmocka0, libcmocka-dev
Architecture: any
Version: 1.1.1-1~nmu1~deb7+2
Maintainer: David Prévot <taffit@debian.org>
Uploaders: Sandro Knauß <hefee@debian.org>
Homepage: http://cmocka.org/
Standards-Version: 4.0.0
Vcs-Browser: https://anonscm.debian.org/cgit/collab-maint/cmocka.git
Vcs-Git: https://anonscm.debian.org/git/collab-maint/cmocka.git
Build-Depends: cmake, debhelper (>= 9)
Package-List: 
 libcmocka-dev deb libdevel extra
 libcmocka0 deb libs extra
Checksums-Sha1: 
 6e872df60804462c3ee1eeb7e9a15538874bca49 85648 cmocka_1.1.1.orig.tar.xz
 4f99d799c9017aa691250ebba045bf905b7c1863 7843 cmocka_1.1.1-1~nmu1~deb7+2.debian.tar.gz
Checksums-Sha256: 
 f02ef48a7039aa77191d525c5b1aee3f13286b77a13615d11bc1148753fc0389 85648 cmocka_1.1.1.orig.tar.xz
 55edfb433467f7f33a7625fcaf4847237cb9d99809126ad12c3c4f271717eb0c 7843 cmocka_1.1.1-1~nmu1~deb7+2.debian.tar.gz
Files: 
 6fbff4e42589566eda558db98b97623e 85648 cmocka_1.1.1.orig.tar.xz
 828eed92c1c50cbd7d1e2b1f49030073 7843 cmocka_1.1.1-1~nmu1~deb7+2.debian.tar.gz
