Format: 3.0 (quilt)
Source: talloc
Binary: libtalloc2, libtalloc2-dbg, libtalloc-dev, python-talloc, python-talloc-dbg, python-talloc-dev
Architecture: any
Version: 2.1.10-2~nmu1~deb7+3
Maintainer: Debian Samba Maintainers <pkg-samba-maint@lists.alioth.debian.org>
Uploaders: Jelmer Vernooĳ <jelmer@debian.org>, Christian Perrier <bubulle@debian.org>, Mathieu Parent <sathieu@debian.org>
Homepage: https://talloc.samba.org/
Standards-Version: 4.0.0
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-samba/talloc.git
Vcs-Git: https://anonscm.debian.org/git/pkg-samba/talloc.git
Build-Depends: debhelper (>= 9), docbook-xml, docbook-xsl, xsltproc, dh-exec, libpopt-dev, python-dev (>= 2.6.6-3)
Package-List: 
 libtalloc-dev deb libdevel optional
 libtalloc2 deb libs optional
 libtalloc2-dbg deb debug extra
 python-talloc deb python optional
 python-talloc-dbg deb debug extra
 python-talloc-dev deb libdevel optional
Checksums-Sha1: 
 4b59061f0ee5f337aacef0c258a4ffd05b3fec6c 441645 talloc_2.1.10.orig.tar.gz
 0af6549d811b213ebada43fa72877246ec59170f 13922 talloc_2.1.10-2~nmu1~deb7+3.debian.tar.gz
Checksums-Sha256: 
 c985e94bebd6ec2f6af3d95dcc3fcb192a2ddb7781a021d70ee899e26221f619 441645 talloc_2.1.10.orig.tar.gz
 401212e6ad872d7a55f5b8c690275034ba505cd53b0264def0a4c7833d0c1efe 13922 talloc_2.1.10-2~nmu1~deb7+3.debian.tar.gz
Files: 
 48b8822a76797bb143e3e38ed738c320 441645 talloc_2.1.10.orig.tar.gz
 b641450ac4977346e58bc714763a8302 13922 talloc_2.1.10-2~nmu1~deb7+3.debian.tar.gz
