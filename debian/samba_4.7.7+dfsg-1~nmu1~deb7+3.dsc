Format: 3.0 (quilt)
Source: samba
Binary: samba, samba-libs, samba-common, samba-common-bin, smbclient, samba-testsuite, registry-tools, libparse-pidl-perl, samba-dev, python-samba, samba-dsdb-modules, samba-vfs-modules, libsmbclient, libsmbclient-dev, winbind, libpam-winbind, libnss-winbind, samba-dbg, libwbclient0, libwbclient-dev, ctdb
Architecture: any all
Version: 2:4.7.7+dfsg-1~nmu1~deb7+3
Maintainer: Debian Samba Maintainers <pkg-samba-maint@lists.alioth.debian.org>
Uploaders: Steve Langasek <vorlon@debian.org>, Jelmer Vernooĳ <jelmer@debian.org>, Ivo De Decker <ivodd@debian.org>, Mathieu Parent <sathieu@debian.org>, Andrew Bartlett <abartlet+debian@catalyst.net.nz>
Homepage: http://www.samba.org
Standards-Version: 4.0.0
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-samba/samba.git
Vcs-Git: https://anonscm.debian.org/git/pkg-samba/samba.git
Build-Depends: bison, debhelper (>> 9), docbook-xml, docbook-xsl, flex, libacl1-dev, libaio-dev [linux-any], libarchive-dev, libattr1-dev, libblkid-dev, libbsd-dev, libcap-dev [linux-any], libcmocka-dev (>= 1.1.1), libcups2-dev, libdbus-1-dev, libgnutls-dev, libgpgme11-dev, libjansson-dev, libldap2-dev, libldb-dev (>= 2:1.2.3~), libncurses5-dev, libpam0g-dev, libparse-yapp-perl, libpcap-dev [hurd-i386 kfreebsd-any], libpopt-dev, libreadline-dev, libtalloc-dev (>= 2.1.9~), libtdb-dev (>= 1.3.14~), libtevent-dev (>= 0.9.33~), perl, pkg-config, po-debconf, python-all-dev (>= 2.6.6-3), python-dnspython, python-ldb (>= 2:1.2.3~), python-ldb-dev (>= 2:1.2.3~), python-talloc-dev (>= 2.1.9~), python-tdb (>= 1.3.14~), python-testtools, xfslibs-dev [linux-any], xsltproc, zlib1g-dev (>= 1:1.2.3)
Package-List: 
 ctdb deb net optional
 libnss-winbind deb net optional
 libpam-winbind deb net optional
 libparse-pidl-perl deb perl optional
 libsmbclient deb libs optional
 libsmbclient-dev deb libdevel optional
 libwbclient-dev deb libdevel optional
 libwbclient0 deb libs optional
 python-samba deb python optional
 registry-tools deb net optional
 samba deb net optional
 samba-common deb net optional
 samba-common-bin deb net optional
 samba-dbg deb debug extra
 samba-dev deb devel optional
 samba-dsdb-modules deb libs optional
 samba-libs deb libs optional
 samba-testsuite deb net optional
 samba-vfs-modules deb net optional
 smbclient deb net optional
 winbind deb net optional
Checksums-Sha1: 
 d2652b946225ea50458a4b436f3f9776be14e42e 16875059 samba_4.7.7+dfsg.orig.tar.gz
 4b5a3357ea7854945dc60c51449d54b14f703124 286803 samba_4.7.7+dfsg-1~nmu1~deb7+3.debian.tar.gz
Checksums-Sha256: 
 29fad16fa70c1342c300d28d1b474b04c01a2a650149e94cace36fcbace80131 16875059 samba_4.7.7+dfsg.orig.tar.gz
 f984946c4f2c86001a662c4de2cf7ec2a26dc153ed68c5ff7f3f8cd4558cee59 286803 samba_4.7.7+dfsg-1~nmu1~deb7+3.debian.tar.gz
Files: 
 e22d9447b9f5eabfcc61b8f9050b99ba 16875059 samba_4.7.7+dfsg.orig.tar.gz
 58d90931fc3f336ddbf39cde1178f45a 286803 samba_4.7.7+dfsg-1~nmu1~deb7+3.debian.tar.gz
