# samba-wheezy

For an local folder use

deb file:[path to debian folder]/ ./

deb-src file:[path to debian folder]/ ./

If you share the folder on an local web server use

deb https://[url for debian folder]/ ./

deb-src https://[url for debian folder]/ ./

To use the gitlab repo direct

deb https://gitlab.com/achim71/samba-wheezy/raw/master/debian/ ./

deb-src https://gitlab.com/achim71/samba-wheezy/raw/master/debian/ ./
